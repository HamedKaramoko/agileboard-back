/**
 * 
 */
package ci.projects.hk.agileboard.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import ci.projects.hk.agileboard.model.StoryBoard;

/**
 * @author hamedkaramoko
 *
 */
@Service
public class StoryBoardServiceImpl implements StoryBoardService{
	
	private List<StoryBoard> storyBoards = Arrays.asList(
			new StoryBoard("RCI", "RCI"),
			new StoryBoard("AgileBoard", "AgileBoard"),
			new StoryBoard("Others", "Others")
			);

	@Override
	public Iterable<StoryBoard> getStoryBoards() {
		return storyBoards;
	}

	@Override
	public StoryBoard getStoryBoard(String label) {
		return storyBoards.stream().filter(sb -> sb.getLabel().equals(label)).findFirst().get();
	}

}
