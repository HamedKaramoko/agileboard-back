/**
 * 
 */
package ci.projects.hk.agileboard.service;

import ci.projects.hk.agileboard.model.StoryBoard;

/**
 * @author hamedkaramoko
 *
 */
public interface StoryBoardService {
	
	Iterable<StoryBoard> getStoryBoards();
	
	StoryBoard getStoryBoard(String label);
}
