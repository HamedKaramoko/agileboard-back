/**
 * 
 */
package ci.projects.hk.agileboard.model;

/**
 * @author hamedkaramoko
 *
 */
public class Activity {
	private Long id;
	private String label;
	private String description;
	private Activity previousActivity;
	private Activity nextActivity;
	private Iterable<Step> steps;
}
