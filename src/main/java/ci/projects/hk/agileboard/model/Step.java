/**
 * 
 */
package ci.projects.hk.agileboard.model;

/**
 * @author hamedkaramoko
 *
 */
public class Step {
	private Long id;
	private String label;
	private String description;
	private Step previousStep;
	private Step nextStep;
	private Iterable<Actor> actors;
	private Iterable<UserStory> userStories;
}
