/**
 * 
 */
package ci.projects.hk.agileboard.model;

/**
 * @author hamedkaramoko
 *
 */
public class UserStory {
	private Long id;
	private String label;
	private String description;
	private Integer priority;
}
