/**
 * 
 */
package ci.projects.hk.agileboard.model;

/**
 * @author hamedkaramoko
 *
 */
public class StoryBoard {
	private Long id;
	private String label;
	private String description;
	private Iterable<Activity> activities;
	
	/**
	 * 
	 */
	public StoryBoard() {
		super();
	}

	/**
	 * @param label
	 */
	public StoryBoard(String label) {
		this(label, "");
	}

	/**
	 * @param label
	 * @param description
	 */
	public StoryBoard(String label, String description) {
		super();
		this.label = label;
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Iterable<Activity> getActivities() {
		return activities;
	}

	public void setActivities(Iterable<Activity> activities) {
		this.activities = activities;
	}
	
}
