/**
 * 
 */
package ci.projects.hk.agileboard.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ci.projects.hk.agileboard.model.StoryBoard;
import ci.projects.hk.agileboard.service.StoryBoardService;

/**
 * @author hamedkaramoko
 *
 */
@RestController
//@RequestMapping("/storyboard")
public class StoryBoardController {
	
	private static Logger logger = LoggerFactory.getLogger(StoryBoardController.class);
	
	@Autowired
	private StoryBoardService storyBoardService;
	
	@GetMapping(path="/list", produces=MediaType.APPLICATION_JSON_VALUE)
	public Iterable<StoryBoard> getStoryBoards(){
		return storyBoardService.getStoryBoards();
	}
	
//	@GetMapping(value="/{label}", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public final StoryBoard getStoryBoard(@PathVariable String label) {
//		
//		StoryBoard found = storyBoardService.getStoryBoard(label);
//        final String username = SecurityContextHolder.getContext().getAuthentication().getName();
//        logger.info(username);
//        return found;
//    }
}
